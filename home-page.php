<?php
/*
* Template Name:Home page
*/
?>


  <!DOCTYPE html>
  <html <?php language_attributes(); ?>>

  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <?php wp_head(); ?>

    <?php $background_cover_image=get_field( 'background_cover_image', option);

          //echo $background_cover_image['url'];

    ?>

      <style>
        video {
          position: fixed;
          top: 50%;
          left: 50%;
          min-width: 100%;
          min-height: 100%;
          width: auto;
          height: auto;
          z-index: -100;
          transform: translateX(-50%) translateY(-50%);
          //background: url('<?php echo $background_cover_image['url'];?>') no-repeat;
          background-size: cover;
          transition: 1s opacity;
          }


          @media(max-width:550px) {
          #home-page-template {
            background: url(<?php echo $background_cover_image['url'];?>) #000 no-repeat center center fixed;
          }
          #bgvid {
            display: none;
          }
        }
      </style>


  </head>



  <body id="home-page-template" <?php body_class(); ?>>

    <video poster="" id="bgvid" playsinline autoplay muted loop>
      <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->

      <source src="<?php the_field('background_video_for_desktop_view_webm', option)?>" type="video/webm">
      <source src="<?php the_field('background_video_for_desktop_view_mp4', option)?>" type="video/mp4">
    </video>



    <div class="header-wrapper">
      <ul>

            <li>
              <?php
                $logo_img = get_field('logo_main', 'option');
                if (!empty($logo_img)):
                    ?>
                <a class="header-logo" href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>" /></a>
                <?php else : {
                    ?>
                  <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                  <?php
                    }
                    endif;
                    ?>
            </li>

            <li class="book-btn">
              <!--<a href="<?php the_field( 'button_url',option ); ?>">
                <?php the_field( 'button_text',option ); ?>
              </a>-->
            </li>

      </ul>
    </div>





    <div class="main-content">
      <div class="container-fluid">

        <div class="row">


          <div id="content" class="main-content-inner col-sm-12" style="padding: 0px;">
            <div class="logo-container">
              <h1 class="text-center"><?php the_field('heading_location', option)?></h1>


              <?php if ( have_rows( 'location_repeater',option ) ) : ?>
                <ul>
                  <?php while ( have_rows( 'location_repeater',option ) ) : the_row(); ?>

                    <li>
                      <a href="<?php the_sub_field( 'link_to_child_site',option ); ?>">
                        <?php $icon_of_location = get_sub_field( 'icon_of_location',option ); ?>
                          <?php if ( $icon_of_location ) { ?>
                            <img src="<?php echo $icon_of_location['url']; ?>" alt="<?php echo $icon_of_location['alt']; ?>" />
                            <?php } ?>
                      </a>
                    </li>
                    <?php endwhile; ?>
                      <?php else : ?>
                      <?php // no rows found ?>
                </ul>
                <?php endif; ?>


            </div>
          </div>
        </div>


      </div>
      <!-- close .container -->
    </div>
    <!-- close .main-content -->




  </body>

  </html>