<?php



function contact_form_subscribe(){ ?>



<?php
}


add_shortcode('skh_contact_form', 'shortcode_skh_contact_form');

function shortcode_skh_contact_form($atts, $content = null) {
    extract( shortcode_atts( array(
		  'variable_1'	=> '',


	), $atts ) );
    ob_start();
?>

<div id="skh_contact_form_wrapper">
<form class="typeset" method="post" action="" accept-charset="UTF-8">

    <input type="hidden" name="action" value="contactForm/sendMessage">
    <input type="hidden" name="redirect" value="/contact">
    <!--<h4 class="col">Questions, Comments, Feedback? Let us Know.</h4>-->

    <div class="inputSize input input--hoshi">
      <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="fromName"><span class="input__label-content input__label-content--hoshi">Name</span></label>
      <input placeholder="your full name" class="input__field input__field--hoshi" type="text" id="fromName" type="text" name="fromName" value="">

    </div>

    <div class="inputSize input input--hoshi">
      <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="fromEmail"><span class="input__label-content input__label-content--hoshi">Email</span></label>
      <input placeholder="your email" class="input__field input__field--hoshi" type="text" id="fromEmail" type="text" name="fromEmail" value="">

    </div>

    <div class="inputSize input input--hoshi">
      <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="phone"><span class="input__label-content input__label-content--hoshi">Phone Number</span></label>
      <input placeholder="your phone number" class="input__field input__field--hoshi" type="text" id="phone" type="text" name="message[Phone]" value="">
    </div>

    <div class="inputSize input input--hoshi">
      <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="location"><span class="input__label-content input__label-content--hoshi">Location</span></label>
      <input placeholder="your location" class="input__field input__field--hoshi" type="hidden" id="location" type="hidden" name="message[Location]" value="">
    </div>

    <div class="inputSize input input--hoshi">

      <select id="locVal" name="toEmail" onchange="insertLocation(this, 0);">
        <option name="message[Services][]" value="info@studiolagree.com">Select a Location</option>
        <option name="message[Services][]" value="info@studiolagree.com">Forest Hill – Toronto, ON</option>
        <option name="message[Services][]" value="info@studiolagree.com">King St. West – Toronto, ON</option>
        <option name="message[Services][]" value="info@studiolagree.com">Vaughan Mills – Vaughan, ON</option>
        <option name="message[Services][]" value="info@studiolagreechicago.com">Lincoln Park – Chicago, IL</option>
        <option name="message[Services][]" value="info@studiolagreechicago.com">Elm Place – Highland Park, IL</option>
        <option name="message[Services][]" value="info@studiolagreeuk.com">London City – London, UK</option>
        <option name="message[Services][]" value="info@studiolagreeuk.com">Guildford – Surrey, UK</option>

      </select>
    </div>
    <div class="inputFull">

      <label for="message"><span class="input__label-content input__label-content--hoshi">Comments or Questions...</span></label>
      <textarea placeholder="comments or questions..." rows="10" cols="40" id="message" name="message[body]"></textarea>
    </div>

    <div class="newsletter col">
      <p>Sign-up for our email list to receive newsletters and the latest information from Studio Lagree.</p>
      <div class="col">


              <input id="checkbox-2" type="checkbox" name="checkbox-2" data-name="Checkbox 2" class="w-checkbox-input">
              <label for="checkbox-2" class="w-form-label body-small">Sign me up!</label>



      </div>
    </div>
    <div class="inputFull">
      <input type="submit" value="submit" onclick="this.disabled=true;this.value='Sending, please wait...';" />


      <h2 id="thanks">Thankyou! Your form has been submitted.</h2>
    </div>
  </form>
</div>

<script>




    function insertLocation(select, row){
     document.getElementById('location').value = select.options[select.selectedIndex].text;

}
</script>

<?php

    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [skh_contact_form][/skh_contact_form]  **/