<?php
/**
 * The template for displaying all single posts.
 *
 * @package Studio Theme
 */

get_header(); ?>
<div style="background-color: #f5f5f5;padding-top:5%;">
<div class="container">
    <div class="row">

        <div id="content" class="main-content-inner col-sm-12">
        <?php echo '<a class="blog_back_btn" href="' . get_permalink( get_option( 'page_for_posts' ) ) . '">Back</a>'; ?>
            <?php while (have_posts()) : the_post(); ?>

                <?php get_template_part('content', 'single'); ?>


            <?php endwhile; // End of the loop. ?>
        </div>
        

</div>
</div>
</div>
<?php get_footer(); ?>
