<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Studio Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <nav class="site-navigation navbar-fixed-top" style="background-color:rgba(253,184,19,.8);">
        <div class="site-container container-fluid" style="padding-bottom: 0px;">
            <div class="row">
                <div class="site-navigation-inner col-sm-12">
                    <div class="navbar navbar-default">
                        <div class="navbar-header">
                            <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->

                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!-- Your site title as branding in the menu -->
                            <?php
                    $logo_img = get_field('logo_header', 'option');
                    if (!empty($logo_img)):
                        ?>
                        <a class="header-logo" href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>" /></a>
                    <?php else : {
                            ?>
                            <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                            <?php
                        }
                    endif;
                    ?>


                        </div>

                        <!-- The WordPress Menu goes here -->

                         <?php
                         if ( has_nav_menu( 'desktop_menu' ) ) :
                         wp_nav_menu(
                                array(
                                    'theme_location' => 'desktop_menu',
                                    'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
                                    'menu_class' => 'nav navbar-nav navbar-right',
                                    'menu_id' => 'main-menu',
                                ) );
                                endif;
                        ?>



                    </div><!-- .navbar -->
                </div>
            </div>
        </div><!-- .container -->
    </nav><!-- .site-navigation -->


<section class="mobile-screen">

<ul class="top-wrapper">
    <li>
                            <!-- Your site title as branding in the menu -->
                            <?php
                    $logo_img = get_field('logo_header', 'option');
                    if (!empty($logo_img)):
                        ?>
                        <a class="header-logo" href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>" /></a>
                    <?php else : {
                            ?>
                            <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                            <?php
                        }
                    endif;
                    ?>
    </li>

        <li >
        <a href="#" class="toggleTopMenu-mobile"></a>
    </li>
    <li class="book-btn mobile-btn">
        <a class="" href="<?php the_field('link_for_the_book_now_button', option);?>"><?php the_field('button_title_mobile', option)?></a>
    </li>

</ul>



<?php
    if ( has_nav_menu( 'mobile_menu' ) ) :
    wp_nav_menu(
            array(
                'theme_location' => 'mobile_menu',
                'container_class' => 'mobile-menu-wrapper',
                'menu_class' => '',
                'menu_id' => 'mobile-menu',
            ) );
    endif;
?>

</section>





<div class="main-content">
	<div class="container-fluid">







