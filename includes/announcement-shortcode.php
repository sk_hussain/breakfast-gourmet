<?php
/*
* Shortcode for Announcements
*/


add_shortcode('skh_announcement', 'skh_announcement_post');

function skh_announcement_post($atts, $content = null) {

    ob_start();

    //echo $refine_category;
    $args = array(
        'post_type' => 'announcement',
        'posts_per_page' => 999,
    );
    $main_post = new WP_Query($args);
    ?>

            <div class="row">
            <?php $i = 1; ?>
            <?php while ($main_post->have_posts()) : $main_post->the_post(); ?>


                    <div class="col-sm-4">
                        <a class="trainer-module__wrapper" href="<?php the_permalink();?>">
                        <div class="trainer-module">
                            <h3 class="trainer-module__title"><?php the_title()?></h3>
                            <div class="trainer-module__img ">
                                <?php $thumb_image = get_field( 'thumbnail_image' ); ?>
                                <?php if ( $thumb_image ) { ?>

                                    <img class="img-resposnsive" src="<?php echo $thumb_image['url']; ?>" alt="<?php echo $thumb_image['alt']; ?>" />

                                <?php } ?>

                            </div>
                        </div>
                        </a>

                    </div>

                    <?php if (0 == ++$m % 3): ?>
                    </div><div class="row">
                    <?php endif; ?>
                    <?php $i = $i + 1; ?>

            <?php endwhile; ?>




    <?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [skh_announcement][/skh_announcement]  **/