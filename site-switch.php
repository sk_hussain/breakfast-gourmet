<?php

function skh_site_switch( $content = null){

// $ukLink = "http://studiolagree.dev/uk/";
// $usLink = "http://studiolagree.dev/us/";
// $gerLink = "http://studiolagree.dev/de/";
// $canLink = "http://studiolagree.dev/ca/";

$ukLink = "http://studiolagreelaunch.com/uk/";
$usLink = "http://studiolagreelaunch.com/us/";
$gerLink = "http://studiolagree.de/";
$canLink = "http://studiolagreelaunch.com/ca/";
?>

            <?php $flag_of_the_country = get_field( 'flag_of_the_country',option ); ?>
            <?php if ( $flag_of_the_country ) { ?>
                <div class="current-site-flag" data-toggle="modal" data-target="#modal-country-switch">
                    <img  src="<?php echo $flag_of_the_country['url']; ?>" alt="<?php echo $flag_of_the_country['alt']; ?>" />
                    <i class="fa fa-angle-down "></i>
                </div>
            <?php } ?>



<div class="modal fade" id="modal-country-switch" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center">CHOOSE YOUR COUNTRY</h4>
      </div>
      <div class="modal-body">




            <ul class="site-switcher-list">

                <!--First-->
                                <li>
                    <a href="<?php echo $canLink;?>">
                    <img src="<?php echo get_stylesheet_directory_uri()?>/includes/canFlag.png" alt="country-flag" />
                    </a>
                </li>



                <!--Second-->
                <li>
                    <a href="<?php echo $usLink;?>">
                    <img src="<?php echo get_stylesheet_directory_uri()?>/includes/USFlag.png" alt="country-flag" />
                    </a>
                </li>

                <!--Third-->

                                <li>
                    <a href="<?php echo $ukLink;?>">
                    <img src="<?php echo get_stylesheet_directory_uri()?>/includes/UKFlag.png" alt="country-flag" />
                    </a>
                </li>



                <!--Fourth-->
                                <li>
                    <a href="<?php echo $gerLink;?>">
                    <img src="<?php echo get_stylesheet_directory_uri()?>/includes/GermFlag.png" alt="country-flag" />
                    </a>
                </li>

            </ul>





      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<?php }