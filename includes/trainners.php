<?php



function my_custom_posttypes() {

    $labels_integratons = array(
        'name'               => 'E-Gallery',
        'singular_name'      => 'E-Gallery',
        'menu_name'          => 'E-Gallery',
        'name_admin_bar'     => 'E-Gallery',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Trainer',
        'new_item'           => 'New E-Gallery',
        'edit_item'          => 'Edit E-Gallery',
        'view_item'          => 'View E-Gallery',
        'all_items'          => 'All E-Gallery',
        'search_items'       => 'Search E-Gallery',
        'parent_item_colon'  => 'Parent E-Gallery:',
        'not_found'          => 'No E-Gallery found.',
        'not_found_in_trash' => 'No E-Gallery found in Trash.',
    );

    $args_integratons = array(
        'labels'             => $labels_integratons,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-universal-access',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'trainers' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'supports'           => array('title', 'editor'),

    );

    register_post_type( 'trainers', $args_integratons );

    $labels_announcement = array(
        'name'               => 'Announcement',
        'singular_name'      => 'Announcement',
        'menu_name'          => 'Announcement',
        'name_admin_bar'     => 'Announcement',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Announcement',
        'new_item'           => 'New Announcement',
        'edit_item'          => 'Edit Announcement',
        'view_item'          => 'View Announcement',
        'all_items'          => 'All Announcement',
        'search_items'       => 'Search Announcement',
        'parent_item_colon'  => 'Parent Announcement:',
        'not_found'          => 'No Announcement found.',
        'not_found_in_trash' => 'No Announcement found in Trash.',
    );

    $args_announcement = array(
        'labels'             => $labels_announcement,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-controls-volumeon',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'announcement' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'supports'           => array('title','editor'),

    );
    register_post_type( 'announcement', $args_announcement );

}
add_action( 'init', 'my_custom_posttypes' );


// Flush rewrite rules to add "review" as a permalink slug
function my_rewrite_flush() {
    my_custom_posttypes();
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );



/*
* Shortcode for trainners page
*/


add_shortcode('skh_trainer', 'skh_trainer_post');

function skh_trainer_post($atts, $content = null) {

    ob_start();

    //echo $refine_category;
    $args = array(
        'post_type' => 'trainers',
        'posts_per_page' => 999,
    );
    $main_post = new WP_Query($args);
    ?>

            <div class="row">
            <?php $i = 1; ?>
            <?php while ($main_post->have_posts()) : $main_post->the_post(); ?>


                    <div class="col-sm-4">
                        <a class="trainer-module__wrapper" href="<?php the_permalink();?>">
                        <div class="trainer-module">
                            <h3 class="trainer-module__title"><?php the_title()?></h3>
                            <div class="trainer-module__img ">
                                <?php $trainer_image = get_field( 'thumbnail_image' ); ?>
                                <?php if ( $trainer_image ) { ?>
                                    <img class="img-resposnsive" src="<?php echo $trainer_image['url']; ?>" alt="<?php echo $trainer_image['alt']; ?>" />
                                <?php } ?>

                            </div>
                        </div>
                        </a>

                    </div>

                    <?php if (0 == ++$m % 3): ?>
                    </div><div class="row">
                    <?php endif; ?>
                    <?php $i = $i + 1; ?>


            <?php endwhile; ?>

            </div>




    <?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [skh_trainer][/skh_trainer]  **/