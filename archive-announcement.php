<?php
/**
* Announcement Archive Page
*
* @package Studio Theme
*/

get_header(); ?>








  <div style="padding-top:100px;">
    <div class="container">

        <div class="row">
          <div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_left secondary-header et_pb_text_0">
            <h3 class="custom-secondary-header">Announcements</h3>
          </div>
        </div>

        <?php echo do_shortcode('[skh_announcement][/skh_announcement] '); ?>




    </div>
  </div>
  <?php get_footer(); ?>