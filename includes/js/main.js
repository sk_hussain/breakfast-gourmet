;
(function ($) {



    goTop();
    //removePreloader();

    $('.menu-item-has-children').hover(
        function () {
            $(this).find('.sub-menu').stop(true).delay(500).slideDown(200);
        },
        function () {
            $(this).find('.sub-menu').stop(true).delay(200).slideUp(100);
        }
    );


    // $( ".dropdown" )
    //   .mouseenter(function() {

    //         $(this).find('.sub-menu').stop(true).delay(500).slideDown(200);

    //   })
    //   .mouseleave(function() {
    //    $(this).find('.sub-menu').stop(true).delay(200).slideUp(100);

    //   });


    // var timeoutId;
    // $(".dropdown").hover(function() {



    //     if (!timeoutId) {
    //         timeoutId = window.setTimeout(function() {
    //             timeoutId = null; // EDIT: added this line
    //             $(this).find('.sub-menu').slideDown(200);
    //        }, 200);
    //     }
    // },
    // function () {
    //     if (timeoutId) {
    //         window.clearTimeout(timeoutId);
    //         timeoutId = null;
    //     }
    //     else {
    //        $(this).find('.sub-menu').slideUp(100);
    //     }
    // });



    // var timeout;

    // $('.dropdown').hover(function(){
    //         timeout=setTimeout(showTooltip,200);
    //     },function(){
    //         hideTooltip();
    //     });

    // function showTooltip() {
    //    $(this).find('.sub-menu').fadeIn('fast');
    //    //alert("hello");
    //    clearTimeout(timeout);
    // }

    // function hideTooltip() {
    //    $(this).find('.dropdown .sub-menu').slideUp(100);
    //   clearTimeout(timeout);
    // }





    function goTop() {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.go-top').addClass('show');
            } else {
                $('.go-top').removeClass('show');
            }
        });

        $('.go-top').on('click', function () {
            $("html, body").animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    }



        $('form').submit(function(ev) {
        // Prevent the form from actually submitting
        ev.preventDefault();

        // Get the post data
        var data = $(this).serialize();

        // Send it to the server
        $.post('/', data, function(response) {
            if (response.success) {
                $('#thanks').fadeIn();
            } else {
                // response.error will be an object containing any validation errors that occurred, indexed by field name
                // e.g. response.error.fromName => ['From Name is required']
                alert('An error occurred. Please try again.');
            }
        });

    });



    //    function removePreloader() {
    //        $('.preloader-wrapper').css('opacity', 0);
    //        setTimeout(function () {
    //            $('.preloader-wrapper').hide();
    //        }, 1000);
    //    }

})(jQuery);