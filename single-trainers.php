<?php
/**
* The template for displaying all single posts.
*
* @package Studio Theme
*/

get_header(); ?>

  <style type="text/css">
    #schedule .hc_footer {
      display: none !important;
    }

    .trainer_teaches_link{
        display:none !important;
    }
  </style>

  <div style="padding-top:8%;">
    <div class="container">
      <div class="row">

        <div id="content" class="main-content-inner col-sm-12 trainer-page">

          <?php while (have_posts()) : the_post(); ?>

            <?php the_content() ?>

              <?php if( !(get_field('hide_this_module')) ): ?>

                <h1 class="title"><?php the_title(); ?></h1>
                <div class="row">
                  <div class="col-sm-3">
                    <?php $thumb_image = get_field( 'thumbnail_image' ); ?>
                      <?php if ( $thumb_image ) { ?>
                        <img class="img-responsive" src="<?php echo $thumb_image['url']; ?>" alt="<?php echo $thumb_image['alt']; ?>" />
                        <?php } ?>


                  </div>

                  <div class="col-sm-9">


                    <div class="nav-tab-wrapper">

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                          <a href="#bio" aria-controls="bio" role="tab" data-toggle="tab">
                            <?php the_field('tab_title_1') ?>
                          </a>
                        </li>
                        <li role="presentation">
                          <a id="schedule_tab" href="#schedule" aria-controls="schedule" role="tab" data-toggle="tab">
                            <?php the_field('tab_title_2') ?>
                          </a>
                        </li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="bio">
                          <?php the_field( 'tab1_content' ); ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="schedule">
                          <?php the_field( 'tab2_content' ); ?>
                        </div>

                      </div>

                    </div>


                  </div>
                </div>

                <?php endif; ?>






                  <?php endwhile; // End of the loop. ?>
        </div>


      </div>
    </div>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function() {

        setTimeout(function(){
    jQuery('.trainer_teaches_link a')[0].click();
}, 3000);



    });
  </script>



  <?php get_footer(); ?>