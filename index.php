<?php
/**
 * The main template file.
 *
 *
 * @package Studio Theme
 */

get_header(); ?>

<div class="row">
    <div id="content" class="main-content-inner col-sm-12" style="padding: 0px;">

<div class="container">
	<div class="row news-blog-post">
	<div class="col-sm-12">
		<h1><?php the_field('news_page_title', option);?></h1>
	</div>
	</div>
</div>



<?php  echo do_shortcode('[jk-sortable posts="999" post_type="post" filter="yes" include="yes"][/jk-sortable]'); ?>

    </div>
</div>





<?php get_footer(); ?>
