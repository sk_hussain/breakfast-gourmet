<?php

function sortable_scripts() {
    wp_enqueue_script('jk-sort-js', get_template_directory_uri() . '/includes/js/isotope.min.js', array('jquery'), '', true);
}
add_action( 'wp_enqueue_scripts', 'sortable_scripts' );





add_shortcode( 'jk-sortable', 'shortcode_sortable' );

/* Shortcode function */
function shortcode_sortable( $atts , $content = null ) {
ob_start();

    extract( shortcode_atts(
        array(
            'posts'         => '999',
            'post_type'     => 'post',
            'include'       => '',
            'filter'        => 'yes',
        ), $atts )
    );

    
    if ($include && $filter == 'yes') {
        $included_terms = explode( ',', $include );
        $included_ids = array();

        foreach( $included_terms as $term ) {
            $term_id = get_term_by( 'slug', $term, 'category')->term_id;
            $included_ids[] = $term_id;
        }

        $id_string = implode( ',', $included_ids );
        $terms = get_terms( 'category', array( 'include' => $id_string ) );

        //Build the filter ?>
<div id="filtered-blog">
    


<div style="background-color:#ffffff">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                        <ul class="project-filter" id="filters">
        <li> <a href="#" data-filter="*">All</a></li>
        <?php $count = count($terms); 
        if ( $count > 0 ){
                foreach ( $terms as $term ) { ?>
                    <li><a href='#' data-filter=.<?php echo $term->slug ?> > <?php echo $term->name?> </a></li>
                <?php }
            }
            ?>

        </ul>
            </div>
        </div>
    </div>
</div>

    
    <?php }
    //Build the layout ?>
<div class="thumb-container-filtered-blog">
<div class="roll-project fullwidth container">
    <div class="project-wrap row" id="isotope-container" data-portfolio-effect="fadeInUp">
        <?php
        $the_query = new WP_Query( array 
        ( 'post_type' => $post_type, 'posts_per_page' => $posts ) );
    while ( $the_query->have_posts() ):
        $the_query->the_post();
        global $post;
        $id = $post->ID;
        $termsArray = get_the_terms( $id, 'category' );
        $termsString = "";
         
        if ( $termsArray) {
            foreach ( $termsArray as $term ) {
                $termsString .= $term->slug.' ';
            }
        } ?>

        <div class="col-sm-4 news-thumbnail project-item item isotope-item <?php echo $termsString; ?> ">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

    <header class="news-header">
    
        <?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
        <span><?php the_time('F jS, Y') ?></span>
        <h3><?php the_field('news_provided_by');?></h3>
    </header><!-- .entry-header -->
    
    
<?php $video_news = get_field('video_url_news'); ?>
    <?php if( !empty($video_news) ): ?>
    <div class="embed-responsive embed-responsive-16by9">
         <iframe class="embed-responsive-item" src="<?php the_field('video_url_news');?>"></iframe>
    </div>
    <?php endif; ?>




    <?php $image_news = get_field('image_for_news'); ?>
    <?php if( empty($video_news) ): ?>
        <img class="img-responsive" src="<?php echo $image_news['url']; ?>" alt="<?php echo $image_news['alt']; ?>" />
    <?php endif; ?>
    
</article><!-- #post-## -->
        </div>

<?php endwhile; ?>

    </div>
</div>
</div>
</div>
<?php

    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

//add_filter( 'widget_text', 'do_shortcode');

/*
 * Sample Shortcode to out put the HTML
 * 
 * [sydney-masonry posts="8" show_all_text="See all" post_type="projects" filter="yes" include=""]
 * [jk-sortable posts="8" show_all_text="See all" post_type="projects" filter="yes" include=""][/jk-sortable]
 * 
 */