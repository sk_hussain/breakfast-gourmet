<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after
*
* @package Studio Theme
*/

?>


  </div>
  <!-- close .container -->
  </div>
  <!-- close .main-content -->

  <a class="go-top"><i class="fa fa-angle-up"></i></a>
  <?php $ftr_image = get_field('ftr_bg_img',option);
    if( !empty($ftr_image) ){
        $get_bg = $ftr_image['url'];
    }
    ?>

  <footer id="main-footer" class="site-footer" style="background-color:#333333;background-image:url(<?php echo $get_bg; ?>);">
    <!--FOOTER BG COLOR -->

    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <h1>
            <?php the_field('title_of_footer', option);?>
          </h1>
        </div>
        <div class="col-md-8 col-md-offset-1">
          <div class="loc-container">
            <div class="row">

              <div class="col-sm-4 col-xs-4">
                <?php $img_1 = get_field('image_for_country_1', option); ?>
                <img src="<?php echo $img_1['url']; ?>" />

                <?php
                    if (have_rows('city_name_1', 'option')):
                        while (have_rows('city_name_1', 'option')) : the_row();
                    ?>
                  <h3>
                    <?php the_sub_field('name_of_the_location_1', 'option'); ?>
                  </h3>

                  <!--Inner Repeater -->
                  <?php
                          if (have_rows('area_main_1', 'option')): ?>
                    <ul class="loc_list">
                      <?php
                              while (have_rows('area_main_1', 'option')) : the_row(); ?>
                        <li>
                          <a href="<?php the_sub_field('url_of_area',option)?>">
                            <?php the_sub_field('name_area', 'option'); ?>
                          </a>
                        </li>
                        <?php
                                endwhile;?>
                    </ul>
                    <?php endif; ?>
                    <!--Inner Repeater End-->
                    <?php
                          endwhile;
                        endif;
                        ?>
              </div>

              <div class="col-sm-4 col-xs-4">
                <?php $img_1 = get_field('image_for_country_2', option); ?>
                <img src="<?php echo $img_1['url']; ?>" />

                <?php
                        if (have_rows('city_name_2', 'option')):
                            while (have_rows('city_name_2', 'option')) : the_row();
                        ?>
                  <h3>
                    <?php the_sub_field('name_of_the_location_2', 'option'); ?>
                  </h3>

                  <!--Inner Repeater -->
                  <?php
                        if (have_rows('area_main_2', 'option')): ?>
                    <ul class="loc_list">
                      <?php
                            while (have_rows('area_main_2', 'option')) : the_row(); ?>
                        <li>
                          <a href="<?php the_sub_field('url_of_area',option)?>">
                            <?php the_sub_field('name_area', 'option'); ?>
                          </a>
                        </li>

                        <?php
                            endwhile;?>
                    </ul>
                    <?php endif; ?>
                    <!--Inner Repeater End-->

                    <?php
                            endwhile;
                          endif;
                          ?>

              </div>

              <div class="col-sm-4 col-xs-4">
                <?php $img_1 = get_field('image_for_country_3', option); ?>
                <img src="<?php echo $img_1['url']; ?>" />

                <?php
                        if (have_rows('city_name_3', 'option')):
                            while (have_rows('city_name_3', 'option')) : the_row();
                        ?>
                  <h3>
                    <?php the_sub_field('name_of_the_location_3', 'option'); ?>
                  </h3>

                  <!--Inner Repeater -->
                  <?php
                            if (have_rows('area_main_3', 'option')): ?>
                    <ul class="loc_list">
                      <?php
                            while (have_rows('area_main_3', 'option')) : the_row(); ?>
                        <li>
                          <a href="<?php the_sub_field('url_of_area',option)?>">
                            <?php the_sub_field('name_area', 'option'); ?>
                          </a>
                        </li>

                        <?php
                            endwhile;?>
                    </ul>
                    <?php endif; ?>
                    <!--Inner Repeater End-->

                    <?php
                            endwhile;
                          endif;
                          ?>

              </div>



              <!-- <div class="col-sm-3 col-xs-3">
                <?php $img_1 = get_field('image_for_country_4', option); ?>
                <img src="<?php echo $img_1['url']; ?>" />

                <?php
                        if (have_rows('city_name_4', 'option')):
                            while (have_rows('city_name_4', 'option')) : the_row();
                        ?>
                  <h3>
                    <?php the_sub_field('name_of_the_location_4', 'option'); ?>
                  </h3>

                 
                  <?php
                        if (have_rows('area_main_4', 'option')): ?>
                    <ul class="loc_list">
                      <?php
                            while (have_rows('area_main_4', 'option')) : the_row(); ?>
                        <li>
                          <a href="<?php the_sub_field('url_of_area',option)?>">
                            <?php the_sub_field('name_area', 'option'); ?>
                          </a>
                        </li>

                        <?php
                            endwhile;?>
                    </ul>
                    <?php endif; ?>
                    

                    <?php
                          endwhile;
                        endif;
                        ?>

              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>


  </footer>
  <!-- close #colophon -->
  <div class="footer-info-bg">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <?php
        $post_objects = get_field('footer_menu', option);

if( $post_objects ): ?>
            <ul class="footer_nav_list">
              <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
              <?php setup_postdata($post); ?>
              <li>
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif;?>

        </div>
        <div class="col-sm-4">
          <?php
if (have_rows('social_repeater', 'option')): ?>
            <ul class="social_icon_list list-unstyled list-inline">
              <?php
while (have_rows('social_repeater', 'option')) : the_row(); ?>
                <li>
                  <a href="<?php the_sub_field('social_url',option)?>">
<i class="<?php the_sub_field('icon_for_social share', 'option'); ?>"></i>
</a>
                </li>

                <?php
endwhile;?>
            </ul>
            <?php endif; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="divider-footer"></div>

          <p>
            <?php the_field('copy_info', option);?>
          </p>
        </div>
      </div>
    </div>
  </div>
  <?php wp_footer(); ?>

  <script type="text/javascript">
    jQuery(document).ready(function () {

      jQuery('.dropdown-toggle').addClass('disabled');
      jQuery('.location-media-thumb  a').addClass('jumper');


//             jQuery("#schedule_tab").click(function(e){
// e.preventDefault();
//    jQuery('.trainer_teaches_link a')[0].click();
//    //return false;
// });




      jQuery(".jumper").on("click", function (e) {

        e.preventDefault();

        jQuery("body, html").animate({
          scrollTop: jQuery(jQuery(this).attr('href')).offset().top
        }, 1000);

      });


      jQuery(".toggleTopMenu-mobile").click(function () {
        jQuery(this).toggleClass("open");
      });

      jQuery(".toggleTopMenu-mobile").click(function () {
        jQuery('.mobile-menu-wrapper').toggleClass("open-menu-mobile");
        jQuery('body').toggleClass("body-overflow");
      });


      // media query event handler
      if (matchMedia) {
        var mq = window.matchMedia("(min-width: 992px)");
        //var linkq = window.matchMedia("(min-width: 768px)");

        //linkq.addListener(LinkChange);
        mq.addListener(WidthChange);
        //LinkChange(linkq);
        WidthChange(mq);
      }

      // media query change
      function WidthChange(mq) {
        if (mq.matches) {
          // window width is at least 991px
          //jQuery('.site-container').addClass("container");
        } else {
          // window width is less than 991px
          jQuery('.site-container').removeClass("container");
          //jQuery('.footer-info-bg .row').addClass('text-center');
          //jQuery('.social_icon_list').removeClass('pull-right');
        }

      }







    });
  </script>





  </body>

  </html>