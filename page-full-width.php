<?php
/* 
 * Template Name:Full width page
 */

get_header();
?>

	
<div class="row">


    <div id="content" class="main-content-inner col-sm-12" style="padding: 0px;">

        <?php while (have_posts()) : the_post(); ?>

            <section id="page-<?php the_ID(); ?>" <?php post_class(); ?> >
                <?php the_content(); ?>
            </section><!-- #post-## -->

        <?php endwhile; // end of the loop.  ?>
            
            
    </div>
</div>


<?php get_footer(); ?>
