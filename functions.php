<?php
/**
 * Studio Theme functions and definitions
 *
 * @package Studio Theme
 */


require get_template_directory() . '/includes/template-settings.php';

if ( ! function_exists( 'studio_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function studio_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Studio Theme, use a find and replace
	 * to change 'studio_theme' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'studio_theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
    add_image_size('sk-2x-thumb', 300);
    add_image_size('sk-3x-thumb', 500, 300, true);
    add_image_size('sk-4x-thumb', 800, 350, true);
    add_image_size('sk-5x-thumb', 2000, 700, true);


	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'desktop_menu' => esc_html__( 'Dektop  Menu', 'studio_theme' ),
		'mobile_menu' => esc_html__( 'Mobile  Menu', 'studio_theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'studio_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // studio_theme_setup
add_action( 'after_setup_theme', 'studio_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function studio_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'studio_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'studio_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function studio_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'studio_theme' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Secondary', 'studio_theme' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );


}
add_action( 'widgets_init', 'studio_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function studio_theme_scripts() {


        // load bootstrap css
        wp_enqueue_style('_s-bootstrap', get_template_directory_uri() . '/includes/css/bootstrap.css');
        // load bootstrap js
        wp_enqueue_script('_s-bootstrapjs', get_template_directory_uri() . '/includes/js/bootstrap.min.js', array('jquery'),'',true);


        //load style css
        wp_enqueue_style( '_s-style', get_stylesheet_uri() );

        // load icheck
        wp_enqueue_style('_s-icheck', get_template_directory_uri() . '/includes/i-check/skins/square/blue.css');
        wp_enqueue_script('_s-icheck', get_template_directory_uri() . '/includes/i-check/icheck.min.js', array('jquery'), '', true);

        // load mian js
        wp_enqueue_script('_s-mainjs', get_template_directory_uri() . '/includes/js/main.js', array('jquery'), '', true);

        // load scroll js
        wp_enqueue_script('_s-scrolljs', get_template_directory_uri() . '/includes/js/jquery.nicescroll.min.js', array('jquery'), '', true);

	   wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	   }


}
add_action( 'wp_enqueue_scripts', 'studio_theme_scripts',12 );



/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/jetpack.php';

/**
* Bootstrap integration
*/
require get_template_directory() . '/includes/functions-strap.php';


/** Hide Admin Bar from Frotn Page  | Hide = '__return_false'  |  Show= '__return_true' */
add_filter('show_admin_bar', '__return_false');

//require get_template_directory() . '/includes/embed-frame/builder.php';


function SKWP_azcods_Modules(){
 if(class_exists("ET_Builder_Module")){
 include("includes/module.php");
 }
}

function Prep_SKWP_azcods_Modules(){
 global $pagenow;

$is_admin = is_admin();
 $action_hook = $is_admin ? 'wp_loaded' : 'wp';
 $required_admin_pages = array( 'edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php', 'export.php' ); // list of admin pages where we need to load builder files
 $specific_filter_pages = array( 'edit.php', 'admin.php', 'edit-tags.php' );
 $is_edit_library_page = 'edit.php' === $pagenow && isset( $_GET['post_type'] ) && 'et_pb_layout' === $_GET['post_type'];
 $is_role_editor_page = 'admin.php' === $pagenow && isset( $_GET['page'] ) && 'et_divi_role_editor' === $_GET['page'];
 $is_import_page = 'admin.php' === $pagenow && isset( $_GET['import'] ) && 'wordpress' === $_GET['import'];
 $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset( $_GET['taxonomy'] ) && 'layout_category' === $_GET['taxonomy'];

if ( ! $is_admin || ( $is_admin && in_array( $pagenow, $required_admin_pages ) && ( ! in_array( $pagenow, $specific_filter_pages ) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page ) ) ) {
 add_action($action_hook, 'SKWP_azcods_Modules', 9789);
 }
}
Prep_SKWP_azcods_Modules();


require get_template_directory() . '/includes/admin/sk-frame-css.php';
require get_template_directory() . '/includes/shortcode.php';
//require get_template_directory() . '/includes/admin-panel-mapping.php';
require get_template_directory() . '/includes/trainners.php';
require get_template_directory() . '/includes/announcement-shortcode.php';
require get_template_directory() . '/includes/contact-form.php';
require get_template_directory() . '/site-switch.php';

/**
 * Adding Builder to the Post.
 */
 function divi_add_new_custom_post_types( $post_types ) {
 $new_post_types_slugs = array(
 'trainers',
 'announcement'
 );
 $post_types = array_merge( $post_types, $new_post_types_slugs );
 return $post_types;
 }
 add_filter( 'et_builder_post_types', 'divi_add_new_custom_post_types' );