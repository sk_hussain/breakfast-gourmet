<?php
/**
 * The main template file.
 *
 *
 * @package Studio Theme
 */

get_header(); ?>
<div class="container">
	<div class="row news-blog-post">
	<div class="col-sm-12">
		<h1><?php the_field('news_page_title', option);?></h1>
	</div>
</div>
</div>

<div style="background-color: #f5f5f5;padding-top:5%;">
	

<div class="container">


    <div class="row">
        
        
	<div id="content" class="main-content-inner col-sm-12">

		<?php if ( have_posts() ) : ?>

			<div class="row">
				
			
			<?php while ( have_posts() ) : the_post(); ?>

				
				<?php
					get_template_part('content', get_post_format());

				?>


			<?php endwhile; ?>

			

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
			</div>
        </div>

</div>
</div>
</div>
<?php get_footer(); ?>
     