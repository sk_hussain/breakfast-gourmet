<?php
/**
 * Template part for displaying single posts.
 *
 * @package Studio Theme
 */

?>
<div class="news_single_page">


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="news-header-single">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<span><?php the_time('F jS, Y') ?></span>
		<h3><?php the_field('news_provided_by');?></h3>
	</header><!-- .entry-header -->

	<?php $video_news = get_field('video_url_news'); ?>
	<?php if( !empty($video_news) ): ?>
	<div class="embed-responsive embed-responsive-16by9">
         <iframe class="embed-responsive-item" src="<?php the_field('video_url_news');?>"></iframe>
    </div>
    <?php endif; ?>




    <?php $image_news = get_field('image_for_news'); ?>
    <?php if( empty($video_news) ): ?>
    	<img class="img-responsive" src="<?php echo $image_news['url']; ?>" alt="<?php echo $image_news['alt']; ?>" />
    <?php endif; ?>


</article><!-- #post-## -->

</div>