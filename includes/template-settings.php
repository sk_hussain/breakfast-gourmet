<?php


/** 1. customize Panel path */
add_filter('acf/settings/path', 'my_acf_settings_path');
function my_acf_settings_path($path) {
    $path = get_stylesheet_directory() . '/includes/admin/panel/';
    return $path;
}

/** 2. customize Panel dir */
add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir($dir) {
    $dir = get_stylesheet_directory_uri() . '/includes/admin/panel/';
    return $dir;
}

/** 3. Hide Panel field group menu item | Hide = '__return_false'  |  Show= '__return_true' */
//add_filter('acf/settings/show_admin', '__return_true');
add_filter('acf/settings/show_admin', '__return_false');

/** 4. Include Panel */
include_once( get_stylesheet_directory() . '/includes/admin/panel/acf.php' );



if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
    'page_title' => 'Theme Settings',
    'menu_title' => 'Theme  Setting',
    'menu_slug' => 'theme_settings',
    'capability' => 'edit_posts',
    'icon_url' => 'dashicons-welcome-view-site',
    'redirect' => false,
    ));
}



if (function_exists('acf_add_local_field_group')):

acf_add_local_field_group(array(
'key' => 'group_561a8d2d4b9a7',
'title' => 'Layout Setting',
'fields' => array(
array(
'key' => 'field_561a8d43c94ec',
'label' => 'Main Section Column',
'name' => 'main_section_column',
'type' => 'number',
'instructions' => '',
'required' => 0,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => '',
'placeholder' => '',
'prepend' => '',
'append' => '',
'min' => '',
'max' => '',
'step' => '',
'readonly' => 0,
'disabled' => 0,
),
array(
'key' => 'field_561a8d70c94ed',
'label' => 'SideBar Column',
'name' => 'sidebar_column',
'type' => 'number',
'instructions' => '',
'required' => 0,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => '',
'placeholder' => '',
'prepend' => '',
'append' => '',
'min' => '',
'max' => '',
'step' => '',
'readonly' => 0,
'disabled' => 0,
),
),
'location' => array(
array(
array(
'param' => 'options_page',
'operator' => '==',
'value' => 'theme_settings',
),
),
),
'menu_order' => 0,
'position' => 'side',
'style' => 'default',
'label_placement' => 'top',
'instruction_placement' => 'label',
'hide_on_screen' => '',
'active' => 1,
'description' => '',
));


endif;

function get_section_column() {
    $default_no = '8';
    $c_no = get_field('main_section_column', 'option');
    if ($c_no):
        echo $c_no;
    else :
        echo $default_no;
    endif;
}

function get_sidebar_column() {
    $default_no = '4';
    $c_no = get_field('sidebar_column', 'option');
    if ($c_no):
        echo $c_no;
    else :
        echo $default_no;
    endif;
}