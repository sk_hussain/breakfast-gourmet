<?php
/**
* Template Name:Trainer Page
*
* @package Studio Theme
*/

get_header(); ?>

  <div class="row">
    <div id="content" class="main-content-inner col-sm-12" style="padding: 0px;">

      <?php while (have_posts()) : the_post(); ?>
        <?php the_content() ?>
          <?php endwhile; // End of the loop. ?>

    </div>
  </div>



  <div>
    <div class="container">

      <?php echo do_shortcode('[skh_trainer][/skh_trainer]'); ?>

    </div>
  </div>


  <?php get_footer(); ?>